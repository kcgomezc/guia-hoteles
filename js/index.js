$(function() {
    $('[data-toggle="tooltip"]').tooltip()
    $('[data-toggle="popover"]').popover()
    $('.carousel').carousel({
        interval: 5000
    });
    $('#exampleModal').on('show.bs.modal', function(e) {
        console.log('el modal se está mostrando');

        $('#contactoBtn').prop('disabled', true);
        $('#contactoBtn').removeClass('btn-primary');

    });
    $('#exampleModal').on('shown.bs.modal	', function(e) {
        console.log('el modal se mostró');
    });
    $('#exampleModal').on('hide.bs.modal	', function(e) {
        console.log('el modal se está ocultando');
    });
    $('#exampleModal').on('hidden.bs.modal		', function(e) {
        console.log('el modal se ocultó');
        $('#contactoBtn').prop('disabled', false);
        $('#contactoBtn').addClass('btn-outline-success');
    });
});